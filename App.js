/* 
1. Екранування - це спосіб знаходження певного символу в рядку. Символ екранування "/"
2. Function Declaration, Function Expression, анонімна функція, стрілкова функція. 
3. Hoisting - це механізм "підняття" змінних і оголошень функцій вверх своєї області видимості.

Як це працює для змінних: 
Якщо ми оголошуємо змінну const, то ми повинні відразу присвоїти їй якесь значення, інакше буде помилка. 
Якщо ми оголошуємо змінну let, а пізніше присвоюємо їй якесь значення, то після оголошення змінна буде - undefined. Якщо спочатку присвоюємо значення, а потім оголошуємо змінну, то буде - is not defined. 

Як це працює для функцій:   
Якщо це Function Declaration, то ми можемо спочатку визвати функцію, а вже потім її об'явити. При Function Expression це неможливо, буде помилка - is not a function.
*/

// Створення нового юзера з перевірками на правильність введення

const createNewUser = () => {
  let firstName = prompt('Введіть ваше ім"я')
  while (!firstName || !isNaN(firstName) || /[\d./,:]/.test(firstName)) {
    firstName = prompt('Введіть ваше ім"я')
  }

  let lastName = prompt('Введіть ваше прізвище')
  while (!lastName || !isNaN(lastName) || /[\d./,:]/.test(lastName)) {
    lastName = prompt('Введіть ваше прізвище')
  }

  let birthDay = prompt('Введіть день вашого народження. Від 1 до 31', '07').toString().padStart(2, '0')
  while (/[a-zA-Zа-яА-Я./,:]/.test(birthDay) || birthDay > 31) {
    birthDay = prompt('Введіть день вашого народження. Від 1 до 31', '07').toString().padStart(2, '0') 
  }

  let birthMonth = prompt('Введіть місяць вашого народження. Від 1 до 12', '03').toString().padStart(2, '0')
  while (/[a-zA-Zа-яА-Я./,:]/.test(birthMonth) || birthMonth > 12) {
    birthMonth = prompt('Введіть місяць вашого народження. Від 1 до 12', '03').toString().padStart(2, '0') 
  }

  let birthYear = prompt('Введіть рік вашого народження', '1998')
  while(/[a-zA-Zа-яА-Я./,:]/.test(birthYear)) {
    birthYear = prompt('Введіть рік вашого народження', '1998') 
  }

  const birthday = `${birthYear}.${birthMonth}.${birthDay}`

  return { firstName, lastName, birthday }
}

const newUser = createNewUser()

// Додавання методів

newUser.getLogin = function () {
  return (
    this.firstName.toLowerCase().substring(0, 1) + this.lastName.toLowerCase()
  )
}

newUser.setFirstName = function (newFirstName) {
  return Object.defineProperty(this, 'firstName', {
    value: newFirstName || this.firstName,
  })
}

newUser.setLastName = function (newLastName) {
  return Object.defineProperty(this, 'lastName', {
    value: newLastName || this.lastName,
  })
}

newUser.getAge = function () {
  let now = new Date()
  let birthday = new Date(this.birthday)
  let month = now.getMonth() - birthday.getMonth()
  let age = now.getFullYear() - birthday.getFullYear()
  
  if (month < 0 || (month === 0 && now.getDate() < birthday.getDate())) {
    age--;
}

  return age
}

newUser.getPassword = function() {
  return this.firstName.toUpperCase().substring(0, 1) + this.lastName.toLowerCase() + new Date(this.birthday).getFullYear()
}

// Налаштування методів

Object.defineProperties(newUser, {
  firstName: {
    writable: false,
    configurable: true,
  },
  lastName: {
    writable: false,
    configurable: true,
  },
  getLogin: {
    enumerable: false,
    writable: false,
  },
  setFirstName: {
    enumerable: false,
    writable: false,
  },
  setLastName: {
    enumerable: false,
    writable: false,
  },
  getAge: {
    enumerable: false,
    writable: false,
  },
  getPassword: {
    enumerable: false,
    writable: false,
  }
})

console.log(newUser)
console.log('Виклик метода getAge -', newUser.getAge())
console.log('Виклик метода getPassword -', newUser.getPassword())